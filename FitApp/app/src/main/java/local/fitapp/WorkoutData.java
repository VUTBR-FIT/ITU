package local.fitapp;

import android.view.ViewParent;
import android.widget.TableRow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Lukas on 13. 12. 2016.
 */

public class WorkoutData {
    private static WorkoutData obj  = null;;
    private List<Map<String, Object>> data = new ArrayList<>();
    public ViewParent rowData = null;
    private WorkoutData(){
    }

    public static WorkoutData getInstance()
    {
        if (obj == null) {
            obj = new WorkoutData();
        }
        return (obj);
    }

    public void generate() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("title", "sss");
        map.put("text", "Barbell Bench-press");
        map.put("img", R.drawable.i360_1);
        data.add(0, map);

        map = new HashMap<String, Object>();
        map.put("text", "Barbell Bench-press");
        map.put("img", R.drawable.i360_1);
        this.data.add(1, map);
        map = new HashMap<String, Object>();
        map.put("text", "Barbell full squat");
        map.put("img", R.drawable.i411);
        data.add(2, map);
        map = new HashMap<String, Object>();
        map.put("text", "Barbell curl");
        map.put("img", R.drawable.i169_1);
        data.add(3, map);
        map = new HashMap<String, Object>();
        map.put("text", "Bent-arm dumbbell pullover");
        map.put("img", R.drawable.i16111);
        data.add(4, map);
        map = new HashMap<String, Object>();
        map.put("text", "Farmer's walk");
        map.put("img", R.drawable.i682_2);
        data.add(5, map);
        map = new HashMap<String, Object>();
        map.put("text", "Barbell Guillotine Bench-press");
        map.put("img", R.drawable.i305_2);
        data.add(6, map);
        map = new HashMap<String, Object>();
        map.put("text", "Barbell incline bench press medium-grip");
        map.put("img", R.drawable.i3311_1);
        data.add(7, map);
        map = new HashMap<String, Object>();
        map.put("text", "Alternating renegade row");
        map.put("img", R.drawable.i506_2);
        data.add(8, map);
        map = new HashMap<String, Object>();
        map.put("text", "Barbell deadlift bent row complex");
        map.put("img", R.drawable.i4861_1);
        data.add(9, map);
        map = new HashMap<String, Object>();
        map.put("text", "Body-up");
        map.put("img", R.drawable.i3991_1);
        data.add(10, map);
    }

    public List<Map<String, Object>> getData(int[] keys)
    {
        List<Map<String, Object>> result = new ArrayList<>();
        for (int i = 0; i < keys.length; i++) {
            Map<String, Object> map = data.get(keys[i]);
            map.put("id", keys[i]);
            result.add(i, map);
        }
        return (result);
    }

}
