package local.fitapp;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by Lukas on 21. 10. 2016.
 */

public class LevelActivity extends BaseActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.level_layout);
    }

    public void pushedButtonLevel(View sender) {
        Button pushed = (Button) sender;

        Button beginner = (Button) findViewById(R.id.level_beginner);
        Button advanced = (Button) findViewById(R.id.level_advanced);
        Button custom = (Button) findViewById(R.id.level_custom);
        Button expert = (Button) findViewById(R.id.level_expert);

        if (pushed == beginner || pushed == advanced || pushed == custom /*|| pushed == expert*/) {

            if (pushed == beginner) user.setLevel(UserData.Level.BEGINNER);
            else if (pushed == advanced) user.setLevel(UserData.Level.ADVANCED);
            else if (pushed == custom) {
                user.setExcercise(-1);
                user.setLevel(UserData.Level.CUSTOM);
                changeActivity(WorkoutActivity.class);
                return;
            }
            else user.setLevel(UserData.Level.EXPOERT);

            changeActivity(ExerciseActivity.class);
        } else notImplementedButton(sender);
    }

}
