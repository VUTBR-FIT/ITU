package local.fitapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.ArrayMap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Created by Lukas on 21. 10. 2016.
 */

public class ExerciseActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.exercises_layout);
        if (user.getType() == UserData.Type.WORKOUT) {
            generateLayout(this.getData());
        } else if (user.getType() == UserData.Type.CARDIO) {

        } else if (user.getType() == UserData.Type.CUSTOM) {

        } else {

        }


    }

    private List<Map<String, Object>> getData()
    {
        List<Map<String, Object>> data = new ArrayList<>();
        if (user.getLevel() == UserData.Level.BEGINNER) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("text", "full body variation 1");
            map.put("img", R.drawable.j2431);
            data.add(0, map);
            map = new HashMap<String, Object>();
            map.put("text", "Variation 1 (chest,lats,triceps)");
            map.put("img", R.drawable.i41012);
            data.add(1, map);
            map = new HashMap<String, Object>();
            map.put("text", "abdominal program 1");
            map.put("img", R.drawable.i20011);
            data.add(2, map);
            map = new HashMap<String, Object>();
            map.put("text", "big arm and shoulder");
            map.put("img", R.drawable.i3081);
            data.add(3, map);
            map = new HashMap<String, Object>();
            map.put("text", "full body variation 2");
            map.put("img", R.drawable.i16312);;
            data.add(4, map);
            map = new HashMap<String, Object>();
            map.put("text", "Variation 2 (legs, triceps)");
            map.put("img", R.drawable.i16111);
            data.add(5, map);
        } else if (user.getLevel() == UserData.Level.ADVANCED) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("text", "full body variation 1");
            map.put("img", R.drawable.j2431);
            data.add(0, map);
            map = new HashMap<String, Object>();
            map.put("text", "Variation 1 (chest,lats,triceps)");
            map.put("img", R.drawable.i41012);
            data.add(1, map);
            map = new HashMap<String, Object>();
            map.put("text", "abdominal program 1");
            map.put("img", R.drawable.i20011);
            data.add(2, map);
            map = new HashMap<String, Object>();
            map.put("text", "big arm and shoulder");
            map.put("img", R.drawable.i3081);
            data.add(3, map);
            map = new HashMap<String, Object>();
            map.put("text", "full body variation 2");
            map.put("img", R.drawable.i16312);
            data.add(4, map);
            map = new HashMap<String, Object>();
            map.put("text", "Variation 2 (legs, triceps)");
            map.put("img", R.drawable.i16111);
            data.add(5, map);
            map = new HashMap<String, Object>();
            map.put("text", "Biceps,triceps");
            map.put("img", R.drawable.i16311);
            data.add(6, map);
            map = new HashMap<String, Object>();
            map.put("text", "Shoulders,backs,abdominals");
            map.put("img", R.drawable.i15612);
            data.add(7, map);
            map = new HashMap<String, Object>();
            map.put("text", "chest,triceps,legs");
            map.put("img", R.drawable.i411);
            data.add(8, map);
        } else if (user.getLevel() == UserData.Level.CUSTOM) {

        } else {

        }
        return (data);
    }
    private void generateLayout(List<Map<String, Object>> data)
    {
        TableRow.LayoutParams row_params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        TableRow.LayoutParams row_lin_params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        TableRow.LayoutParams row_lin_lin_params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        TableRow.LayoutParams row_lin_text_params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, this.dpToPixels(3));
        TableRow.LayoutParams row_lin_lin_image_params = new TableRow.LayoutParams(this.dpToPixels(30), (int) getResources().getDimension(R.dimen.textSize));
        TableRow.LayoutParams row_lin_lin_button_params = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, (int) getResources().getDimension(R.dimen.textSize));

        LinearLayout table = (LinearLayout) findViewById(R.id.table_excercise);
        table.removeAllViews();

        for (int i = 0; i < data.size(); i++) {
            TableRow row = new TableRow(this);
            row.setLayoutParams(row_params);

            LinearLayout row_lin = new LinearLayout(this);
            row_lin.setLayoutParams(row_lin_params);
            row_lin.setOrientation(LinearLayout.VERTICAL);

            LinearLayout row_lin_lin = new LinearLayout(this);
            row_lin_lin.setLayoutParams(row_lin_lin_params);
            row_lin_lin.setOrientation(LinearLayout.HORIZONTAL);

            Button row_lin_lin_button = new Button(this);
            row_lin_lin_button.setLayoutParams(row_lin_lin_button_params);
            row_lin_lin_button.setPadding(this.dpToPixels(5),0,0,0);
            row_lin_lin_button.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
            row_lin_lin_button.setBackground(getDrawable(R.color.trans));
            row_lin_lin_button.setText((String) data.get(i).get("text"));
            row_lin_lin_button.setTextColor(Color.WHITE);
            row_lin_lin_button.setId(i);
            if (i < 2) {
                row_lin_lin_button.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        showDetailWorkout(v);
                    }
                });
            } else {
                row_lin_lin_button.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        notImplementedButton(v);
                    }
                });
            }

            ImageView row_lin_lin_image = new ImageView(this);
            row_lin_lin_image.setLayoutParams(row_lin_lin_image_params);
            row_lin_lin_image.setBackgroundResource((int) data.get(i).get("img"));

            TextView row_lin_text = new TextView(this);
            row_lin_text.setLayoutParams(row_lin_text_params);
            row_lin_text.setBackground(getDrawable(R.color.borderButton));

            row_lin_lin.addView(row_lin_lin_image);
            row_lin_lin.addView(row_lin_lin_button);
            row_lin.addView(row_lin_lin);
            row_lin.addView(row_lin_text);
            row.addView(row_lin);
            table.addView(row);
        }
    }

    public void showDetailWorkout(View sender) {
        user.setExcercise((int) sender.getId());
        changeActivity(WorkoutActivity.class);
    }
}
