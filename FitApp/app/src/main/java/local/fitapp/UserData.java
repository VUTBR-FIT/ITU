package local.fitapp;

/**
 * Created by Lukas on 14. 10. 2016.
 */

public class UserData {
    private static UserData obj = null;

    private String userName = "";
    private String firstName;
    private String lastName;
    private int weight = -1;
    private int height = -1;
    private int genger = 1;

    private Level levelTraining;
    private Type typeTraining;

    private int excercise;

    public String getUsername() {
        return (this.userName);
    }

    public void setUsername(String name) {
        this.userName = name;
    }

    private UserData(){}

    public static UserData getInstance()
    {
        if (obj == null) {
            obj = new UserData();
        }
        return (obj);
    }

    public Type getType() {
        return (this.typeTraining);
    }

    public void setType(Type type) {
        this.typeTraining = type;
    }

    public Level getLevel() {
        return (this.levelTraining);
    }

    public void setLevel(Level level) {
        this.levelTraining = level;
    }

    public String getFirstName() {
        return (firstName);
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return (lastName);
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getWeight() {
        return (this.weight);
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return (this.height);
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getGenger() {
        return (this.genger);
    }

    public void setGenger(int genger) {
        this.genger = genger;
    }

    public void setExcercise(int x)  {
        this.excercise = x;
    }

    public int getExcercise()  {
        return (this.excercise);
    }

    public enum Type {
        WORKOUT,
        CARDIO,
        CUSTOM,
        MY_WORKOUT,
    }

    public enum Level {
        BEGINNER,
        ADVANCED,
        CUSTOM,
        EXPOERT
    }


}
