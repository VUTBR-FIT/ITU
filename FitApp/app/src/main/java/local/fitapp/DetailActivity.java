package local.fitapp;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Lukas on 21. 10. 2016.
 */

public class DetailActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.detail_layout);
        generateLayout(getData());
    }

    private Map<String, Object> getData()
    {
        List<Map<String, Object>> data = new ArrayList<>();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("title", "Barbell Bench-Press");
        map.put("text", "Lie back on the flat bench. Hold the barbell with overhand grip. Distance between your hands is slighter wider than shoulder width. Hold the barbell with arms fully extend. The barbell is directly above the center of your chest.");
        map.put("img", R.drawable.i360_1);
        data.add(0, map);
        map = new HashMap<String, Object>();
        map.put("title", "Barbell Full Squat");
        map.put("text", "1) This exercise is best performed inside a squat rack for safety purposes. To begin, first set the bar on a rack just above shoulder level. Once the correct height is chosen and the bar is loaded, step under the bar and place the back of your shoulders (slightly below the neck) across it.\n" +
                "2) Hold on to the bar using both arms at each side and lift it off the rack by first pushing with your legs and at the same time straightening your torso.\n" +
                "3) Step away from the rack and position your legs using a shoulder-width medium stance with the toes slightly pointed out. Keep your head up at all times and maintain a straight back. This will be your starting position.\n" +
                "4) Begin to slowly lower the bar by bending the knees and sitting back with your hips as you maintain a straight posture with the head up. Continue down until your hamstrings are on your calves. Inhale as you perform this portion of the movement.\n" +
                "5) Begin to raise the bar as you exhale by pushing the floor with the heel or middle of your foot as you straighten the legs and extend the hips to go back to the starting position.\n" +
                "6) Repeat for the recommended amount of repetitions.");
        map.put("img", R.drawable.i64_2);
        data.add(1, map);
        map = new HashMap<String, Object>();
        map.put("title", "Barbell Curl");
        map.put("text", "1) Stand up with your torso upright while holding a barbell at a shoulder-width grip. The palm of your hands should be facing forward and the elbows should be close to the torso. This will be your starting position.\n" +
                "2) While holding the upper arms stationary, curl the weights forward while contracting the biceps as you breathe out. Tip: Only the forearms should move.\n" +
                "3) Continue the movement until your biceps are fully contracted and the bar is at shoulder level. Hold the contracted position for a second and squeeze the biceps hard.\n" +
                "4) Slowly begin to bring the bar back to starting position as your breathe in.\n" +
                "5) Repeat for the recommended amount of repetitions.");
        map.put("img", R.drawable.i169_1);
        data.add(2, map);

        return (data.get(user.getExcercise()));
    }
    private void generateLayout(Map<String, Object> data)
    {
        ((ImageView) findViewById(R.id.detail_img)).setImageResource((int) data.get("img"));
        ((TextView) findViewById(R.id.detail_title)).setText((String) data.get("title"));
        ((TextView) findViewById(R.id.detail_text)).setText((String) data.get("text"));
    }
}
