package local.fitapp;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

/**
 * Created by Lukas on 21. 10. 2016.
 */

public class ProfileActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.profile_layout);

        EditText firstName = (EditText) findViewById(R.id.profile_firstName);
        firstName.setText(user.getFirstName());

        EditText lastName = (EditText) findViewById(R.id.profile_lastName);
        lastName.setText(user.getLastName());

        RadioButton male = (RadioButton) findViewById(R.id.profile_male);
        RadioButton female = (RadioButton) findViewById(R.id.profile_female);

        if (user.getGenger() == 1) male.setChecked(true);
        else female.setChecked(true);

        EditText weight = (EditText) findViewById(R.id.profile_weight);
        if (user.getWeight() != -1) weight.setHint(user.getWeight() + " kg");

        EditText height = (EditText) findViewById(R.id.profile_height);
        if (user.getHeight() != -1) height.setHint(user.getHeight() + " cm");

        firstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                UserData user = UserData.getInstance();
                user.setFirstName(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        lastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                UserData user = UserData.getInstance();
                user.setLastName(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        weight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                UserData user = UserData.getInstance();
                user.setWeight(Integer.parseInt(s.toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        height.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                UserData user = UserData.getInstance();
                user.setHeight(Integer.parseInt(s.toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public void checkedGender(View sender) {
        RadioButton checked = (RadioButton) sender;

        RadioButton male = (RadioButton) findViewById(R.id.profile_male);
        RadioButton female = (RadioButton) findViewById(R.id.profile_female);

        if (checked == male) {
            female.setChecked(false);
            male.setChecked(true);
            user.setGenger(1);
        } else {
            male.setChecked(false);
            female.setChecked(true);
            user.setGenger(0);
        }
    }
}
