package local.fitapp;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.widget.CompoundButtonCompat;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.logging.LogRecord;

/**
 * Created by Lukas on 21. 10. 2016.
 */

public class WorkoutActivity extends BaseActivity {

    private long sec, min, hr, startTime, elapsedTime;
    private String sSec, sMin, sHr;
    private boolean stopped = false;
    private final int REFRESH_TIME = 1000;
    private Handler mHandler = new Handler();

    private Runnable startStopWatch = new Runnable() {
        @Override
        public void run() {
            elapsedTime = System.currentTimeMillis() - startTime;
            updateWatch(elapsedTime);
            mHandler.postDelayed(this, REFRESH_TIME);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.workout_layout);

        if (user.getType() == UserData.Type.WORKOUT) {
            generateLayout(this.getWorkoutData());
        } else if (user.getType() == UserData.Type.CARDIO) {

        } else if (user.getType() == UserData.Type.CUSTOM) {
            generateLayout(this.getWorkoutData());
        } else {

        }
    }

    private List<Map<String, Object>> getWorkoutData()
    {
        List<Map<String, Object>> data = new ArrayList<>();
        int[] keys;
        WorkoutData dataWorkout = WorkoutData.getInstance();
        dataWorkout.rowData = null;
        if (user.getExcercise() == 0) {
            keys = new int[5];
            keys[0] = 1;
            keys[1] = 2;
            keys[2] = 3;
            keys[3] = 4;
            keys[4] = 5;


            dataWorkout.generate();

            data = dataWorkout.getData(keys);

            Map<String, Object> map = new HashMap<String, Object>();
            map.put("title", "Full body variation");
            map.put("img", 1);
            data.add(0, map);
            return (data);
        } else if (user.getExcercise() == 1) {
            keys = new int[5];
            keys[0] = 6;
            keys[1] = 7;
            keys[2] = 8;
            keys[3] = 9;
            keys[4] = 10;

            dataWorkout.generate();

            data = dataWorkout.getData(keys);

            Map<String, Object> map = new HashMap<String, Object>();
            map.put("title", "Variation 1 (CHEST, LATS, TRICEPS)");
            map.put("img", 1);
            data.add(0, map);
            return (data);
        } else if (user.getExcercise() == -1) {
            keys = new int[10];
            keys[0] = 1;
            keys[1] = 2;
            keys[2] = 3;
            keys[3] = 4;
            keys[4] = 5;
            keys[5] = 6;
            keys[6] = 7;
            keys[7] = 8;
            keys[8] = 9;
            keys[9] = 10;

            dataWorkout.generate();

            data = dataWorkout.getData(keys);

            Map<String, Object> map = new HashMap<String, Object>();
            map.put("title", "CUSTOM");
            map.put("img", 1);
            data.add(0, map);

        }
        return(data);
    }
    private void generateLayout(List<Map<String, Object>> data)
    {
        TableRow.LayoutParams row_params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        TableRow.LayoutParams row_lin_params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        TableRow.LayoutParams row_lin_lin_params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        TableRow.LayoutParams row_lin_text_params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, this.dpToPixels(3), 1f);
        TableRow.LayoutParams row_lin_lin_text_params = new TableRow.LayoutParams(this.dpToPixels(40), this.dpToPixels(35), 1f);
        TableRow.LayoutParams row_lin_lin_image_params = new TableRow.LayoutParams(this.dpToPixels(20), this.dpToPixels(35), 1f);
        TableRow.LayoutParams row_lin_lin_check_params = new TableRow.LayoutParams(this.dpToPixels(50), this.dpToPixels(35), 1f);
        TableRow.LayoutParams row_lin_lin_button_params = new TableRow.LayoutParams(this.dpToPixels(140), this.dpToPixels(35), 1f);
        TableRow.LayoutParams row_lin_lin_check01_params = new TableRow.LayoutParams(this.dpToPixels(20), this.dpToPixels(35), 1f);

        LinearLayout table = (LinearLayout) findViewById(R.id.table_workout);

        ScrollView scroll = (ScrollView) table.getParent();
        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        scroll.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, size.y - 1000));

        table.removeAllViews();


        for (int i = 0; i < data.size(); i++) {
            TableRow row = new TableRow(this);
            row.setLayoutParams(row_params);

            if (i == 0) {
                TextView title = new TextView(this);
                title.setText((String) data.get(i).get("title"));
                title.setTextColor(Color.WHITE);
                title.setTextSize(19f);

                row.addView(title);
                ((LinearLayout) ((ScrollView)findViewById(R.id.scroll_workout)).getParent()).addView(row, 0);
                continue;
            }

            LinearLayout row_lin = new LinearLayout(this);
            row_lin.setOrientation(LinearLayout.VERTICAL);
            row_lin.setLayoutParams(row_lin_params);

            LinearLayout row_lin_lin = new LinearLayout(this);
            row_lin_lin.setOrientation(LinearLayout.HORIZONTAL);
            row_lin_lin.setLayoutParams(row_lin_lin_params);
            row_lin_lin.setWeightSum(4);

            CheckBox row_lin_lin_check01 = new CheckBox(this);
            row_lin_lin_check01.setLayoutParams(row_lin_lin_check01_params);
            row_lin_lin_check01.setChecked(true);
            row_lin_lin_check01.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked == false) {
                        LinearLayout table = (LinearLayout) findViewById(R.id.table_workout);
                        WorkoutData data = WorkoutData.getInstance();
                        if (data.rowData == null) {

                            TableRow row = new TableRow(getApplicationContext());
                            row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, dpToPixels(20)));
                            table.addView(row);
                            row = new TableRow(getApplicationContext());
                            row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, dpToPixels(50)));
                            Button btn = new Button(getApplicationContext());
                            btn.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, dpToPixels(30)));
                            btn.setBackground(getDrawable(R.drawable.input_white));
                            btn.setTextSize(14f);
                            btn.setTextColor(Color.BLACK);
                            btn.setPadding(0, 0, 0, 0);
                            btn.setText("undo");
                            btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    WorkoutData data = WorkoutData.getInstance();
                                    LinearLayout table = (LinearLayout) findViewById(R.id.table_workout);
                                    table.removeView(table.getChildAt(table.getChildCount() - 2));
                                    table.removeView((View) (v.getParent()));
                                    table.addView((View) data.rowData);

                                    data.rowData = null;
                                }
                            });
                            row.addView(btn);
                            table.addView(row);
                        }
                        ((CheckBox) buttonView).setChecked(true);
                        data.rowData = buttonView.getParent().getParent().getParent();
                        table.removeView((View) (buttonView.getParent().getParent().getParent()));
                    }
                }
                public int dpToPixels(float height) {
                    return ((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, height, getResources().getDisplayMetrics()));
                }

            });

            Button row_lin_lin_button = new Button(this);
            row_lin_lin_button.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
            row_lin_lin_button.setLayoutParams(row_lin_lin_button_params);
            row_lin_lin_button.setBackground(getDrawable(R.color.trans));
            row_lin_lin_button.setText((String) data.get(i).get("text"));
            row_lin_lin_button.setTextColor(Color.WHITE);
            row_lin_lin_button.setId(i);
            row_lin_lin_button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    showDetailWorkout(v);
                }
            });

            TextView row_lin_lin_text = new TextView(this);
            row_lin_lin_text.setLayoutParams(row_lin_lin_text_params);
            row_lin_lin_text.setTextColor(Color.WHITE);
            if (UserData.Level.BEGINNER == user.getLevel())
                row_lin_lin_text.setText("Series 3");
            else row_lin_lin_text.setText("Series 4");
            CheckBox row_lin_lin_check = new CheckBox(this);
            //TODO
            int[] colors = {R.color.colorAccent, R.color.colorAccent};
            int[][] states = {{1, 0}};
            row_lin_lin_check.setBackgroundTintList(new ColorStateList(states, colors));
            row_lin_lin_check.setLayoutParams(row_lin_lin_check_params);
            row_lin_lin_check.setTextColor(Color.WHITE);
            row_lin_lin_check.setText("done");


            ImageView row_lin_lin_image = new ImageView(this);
            row_lin_lin_image.setLayoutParams(row_lin_lin_image_params);
            row_lin_lin_image.setBackground(getDrawable((int) data.get(i).get("img")));

            TextView row_lin_text = new TextView(this);
            row_lin_text.setLayoutParams(row_lin_text_params);
            row_lin_text.setBackground(getDrawable(R.color.borderButton));


            if (user.getExcercise() == -1) row_lin_lin.addView(row_lin_lin_check01);
            row_lin_lin.addView(row_lin_lin_button);
            if (user.getExcercise() != -1)row_lin_lin.addView(row_lin_lin_text);
            row_lin_lin.addView(row_lin_lin_check);
            row_lin_lin.addView(row_lin_lin_image);
            row_lin.addView(row_lin_lin);
            row_lin.addView(row_lin_text);
            row.addView(row_lin);
            table.addView(row);
        }
    }

    public void showDetailWorkout(View sender) {
        user.setExcercise((int) sender.getId());
        changeActivity(DetailActivity.class);
    }

    public void watchStart(View sender) {
        Button btn = (Button) sender;
        if (stopped) startTime = System.currentTimeMillis() - elapsedTime;
        else startTime = System.currentTimeMillis();

        mHandler.removeCallbacks(startStopWatch);
        mHandler.postDelayed(startStopWatch, 0);


    }
    public void watchStop(View sender) {
        Button btn = (Button) sender;
        mHandler.removeCallbacks(startStopWatch);
        if (stopped) {
            ((TextView)findViewById(R.id.watch)).setText("00:00:00");
            stopped = false;
        } else stopped = true;
    }

    private void updateWatch(float time) {
        time = time/1000;
        sec = (long) time;
        min = (long) time/60;
        hr = (long) time/60/60;

        sec = sec % 60;
        min = min % 60;

        if (sec == 0) sSec = "00";
        else if (sec < 10) sSec = "0" + String.valueOf(sec);
        else sSec = String.valueOf(sec);

        if (min == 0) sMin = "00";
        else if (min < 10) sMin = "0" + String.valueOf(min);
        else sMin = String.valueOf(min);

        if (hr < 10 ) sHr = "0" + String.valueOf(hr);
        else sHr = String.valueOf(hr);

        ((TextView)findViewById(R.id.watch)).setText(sHr + ":" + sMin + ":" + sSec);
    }

}
