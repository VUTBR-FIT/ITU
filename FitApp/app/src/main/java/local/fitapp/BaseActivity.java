package local.fitapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;

/**
 * Created by Lukas on 20. 10. 2016.
 */

public class BaseActivity extends AppCompatActivity {

    protected UserData user = UserData.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Skrytí horní lišty
        getSupportActionBar().hide();

        // Zblokování změny orientace
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
    }

    public void notImplementedButton(View sender) {
        AlertDialog dialog = new AlertDialog.Builder(this).create();
        dialog.setTitle("NOT IMPLEMENTED");
        dialog.setMessage("Toto tlačítko není implementováno.");
        dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    protected void changeActivity(Class<?> cls) {
        Intent newActivity = new Intent(this, cls);
        startActivity(newActivity);
    }


    public void pushedButtonProfile(View sender) {
        Button pushed = (Button) sender;

        Button profile = (Button) findViewById(R.id.button_profile);

        if (pushed == profile) {
            changeActivity(ProfileActivity.class);
        } else notImplementedButton(sender);
    }

    public void pushedButtoneType(View sender) {
        Button pushed = (Button) sender;

        Button workout = (Button) findViewById(R.id.cat_workout);
        Button cardio = (Button) findViewById(R.id.cat_cardio);
        Button custom = (Button) findViewById(R.id.cat_custom);
        Button my_workout = (Button) findViewById(R.id.cat_myworkout);

        Button stats = (Button) findViewById(R.id.button_stats);
        //Button profile = (Button) findViewById(R.id.button_profile);

        if (pushed == workout /*|| pushed == cardio*/ || pushed == custom || pushed == my_workout) {
            if (pushed == workout) user.setType(UserData.Type.WORKOUT);
            else if (pushed == cardio) user.setType(UserData.Type.CARDIO);
            else if (pushed == custom) {
                user.setType(UserData.Type.CUSTOM);
                user.setLevel(UserData.Level.CUSTOM);
                user.setExcercise(-1);
                changeActivity(WorkoutActivity.class);
                return;
            } else {
                user.setType(UserData.Type.MY_WORKOUT);
                changeActivity(MyWorkoutActivity.class);
                return;
            }
            changeActivity(LevelActivity.class);
        } else if (stats == pushed) {
            changeActivity(StatisticActivity.class);
        }

        else notImplementedButton(sender);
    }

    protected int dpToPixels(float height) {
        return ((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, height, getResources().getDisplayMetrics()));
    }
}
