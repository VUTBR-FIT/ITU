package local.fitapp;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by Lukas on 20. 10. 2016.
 */

public class MainMenuActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_menu_layout);
    }
}
