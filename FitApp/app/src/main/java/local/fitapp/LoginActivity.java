package local.fitapp;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class LoginActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login_layout);
    }

    public void pushedButtonLogin(View sender) {
        Button pushed = (Button) sender;

        Button login = (Button) findViewById(R.id.buttonLogin);

        if (pushed == login) {
            user.setUsername(findViewById(R.id.editText).toString());

            changeActivity(MainMenuActivity.class);
        } else notImplementedButton(sender);
    }

}
